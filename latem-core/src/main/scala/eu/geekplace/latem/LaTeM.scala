package eu.geekplace.latem

import scala.collection._
import scala.collection.JavaConverters._
import scala.util.Using

import java.nio.file.Files

import org.eclipse._
import org.eclipse.jgit.api._
import org.eclipse.jgit.storage.file.FileRepositoryBuilder

private case class Symlink(
    name: os.Path,
    potentiallyRelativeTarget: java.nio.file.Path,
) {
  val realTarget: java.nio.file.Path = name.toNIO.toRealPath()
  val target: os.Path =
    os.Path((name / os.up).toNIO.resolve(potentiallyRelativeTarget))

  override def toString() = {
    var res = s"${name} → ${potentiallyRelativeTarget}"
    if (!potentiallyRelativeTarget.isAbsolute()) {
      res += s" (${target})"
    }
    res
  }
}

trait GitReference
private case class Repository(
    headId: jgit.lib.ObjectId,
    remoteUrl: jgit.transport.URIish,
    git: Git,
) extends GitReference
private case class Submodule(
    path: os.Path,
    relativePath: String,
    id: org.eclipse.jgit.lib.ObjectId,
    remoteUrl: String,
) extends GitReference

object LaTeM {

  private def getGitFor(dir: os.Path): Git = {
    val repo = new FileRepositoryBuilder()
      .findGitDir(dir.toIO)
      .build()
    new Git(repo)
  }

  private def closeGit(git: Git) = {
    // Sadly, Git.close() will not close the repository, so we have to
    // do it additionally.
    git.getRepository().close()
    git.close()
  }

  def instantiateTemplate(configuration: Configuration): Unit = {
    val templateDirectory = configuration.templateDirectory
    val destinationDirectory = configuration.destinationDirectory

    if (!templateDirectory.toIO.exists)
      throw new Exception(
        s"Template directory ${templateDirectory} does not exist"
      )
    if (!templateDirectory.toIO.isDirectory)
      throw new Exception(
        s"Template directory ${templateDirectory} is not a directory"
      )
    if (destinationDirectory.toIO.exists())
      throw new Exception(s"${destinationDirectory} exists")

    println(
      s"Instanciating template from ${templateDirectory} into ${destinationDirectory}"
    )

    // TODO: Use scala.utilUsing or ensure otherwise that templateGit
    // is closed in the end. Although that is not really important.
    val (templateGit, templateGitRootDirectory) = {
      val git = getGitFor(templateDirectory)
      val templateGitRepository = git.getRepository()
      val templateGitRootDirectory =
        os.Path(templateGitRepository.getWorkTree())
      val templateGitHeadId =
        templateGitRepository.resolve(jgit.lib.Constants.HEAD)
      println(s"Template git: ${templateGitRepository}")
      println(s"worktree: ${templateGitRootDirectory}")

      val templateGitUrl = {
        // TOOD: Better error reporting in case there are no remotes
        // at all defined.
        val remotes = git.remoteList().call().asScala
        remotes
          .collectFirst {
            case remote if remote.getName() == "origin" => remote
          }
          // If no remote named 'origin' exists, return the first remote (if any).
          .orElse(remotes.headOption)
          .map(remote => remote.getURIs().asScala.headOption)
          .get
          .get
      }

      (
        Repository(templateGitHeadId, templateGitUrl, git),
        templateGitRootDirectory,
      )
    }

    val submodules = {
      val submodulesMapBuilder =
        immutable.Map.newBuilder[os.Path, Submodule]

      Using(
        org.eclipse.jgit.submodule.SubmoduleWalk
          .forIndex(templateGit.git.getRepository())
      ) { walk =>
        while (walk.next()) {
          val relativePath = walk.getModulesPath()
          val objectId = walk.getObjectId()
          val url = walk.getRemoteUrl()

          println(s"Found submodule at ${relativePath}")
          println(s"objectId: ${objectId}")
          println(s"url: ${url}")
          println(s"update: ${walk.getModulesUpdate()}")

          val path = os.Path(relativePath, templateGitRootDirectory)

          val submodule = Submodule(path, relativePath, objectId, url)
          submodulesMapBuilder += (path -> submodule)
        }
      }

      submodulesMapBuilder.result()
    }

    // TODO: The curly braces" after 'for' and before 'yield' could be
    // ommited in Scala 3, but the scalafmt version of the
    // gradle-scalaftm plugin does not yet support this.
    val symlinks: immutable.Map[os.Path, Symlink] =
      (
        for {
          templatePath <- os.walk(templateDirectory)
          templatePathNio = templatePath.toNIO
          if Files.isSymbolicLink(templatePathNio)
          target = Files.readSymbolicLink(templatePathNio)
        } yield (templatePath -> Symlink(templatePath, target))
      ).to(immutable.Map)

    val symlinksToSourceMap = {
      val map = immutable.Map.newBuilder[Symlink, GitReference]

      for {
        symlink <- symlinks.values
      } {
        if (!symlink.target.startsWith(templateGitRootDirectory))
          throw new Exception(
            s"Symlink ${symlink} outside of git directory ${templateGitRootDirectory} of the template"
          )

        println(s"symlink: ${symlink}")

        submodules
          .find((submodulePath, _) => symlink.target.startsWith(submodulePath))
          .map(_._2)
          .orElse(
            if (symlink.target.startsWith(templateDirectory)) then None
            else Some(templateGit)
          )
          .foreach(submodule => map += (symlink -> submodule))
      }
      map.result()
    }

    val submodulesInTemplateDirectory = submodules
      .filter((path, submodule) => path.startsWith(templateDirectory))
      .map((path, submodule) => (submodule, path.relativeTo(templateDirectory)))

    val sourcesOutsideTemplateDirectoryToSymlinksMap =
      // TODO: In Scala3 the surrounding braces can be removed, but
      // this is not yet supported by the currently used scalafmt version.
      (symlinksToSourceMap.groupMap(_._2)(_._1)
        -- submodulesInTemplateDirectory.keySet)

    // TODO: Add santiy check that the last path component of every
    // submdoule outside the template directory is unique. Otherwise,
    // the name will clash if we try to add the submdoule to
    // $destDir/submodules/.

    os.makeDir.all(destinationDirectory)
    val destinationGit =
      Git.init().setDirectory(destinationDirectory.toIO).call()

    // Copy template files into the destination directory.
    for {
      templatePath <- os.walk(templateDirectory)
      if (templatePath.toIO.isFile)
      // Ensure that the file is not part of a git submodule.
      if (submodulesInTemplateDirectory.forall((submodule, _) =>
        !templatePath.startsWith(submodule.path)
      ))
      // Ensure that the file is not a known symlink.
      if (!symlinks.contains(templatePath))
      relativePathInTemplateDirectory = templatePath.relativeTo(
        templateDirectory
      )
      destination = destinationDirectory / relativePathInTemplateDirectory
    } {
      os.copy(
        templatePath,
        destination,
        followLinks = false,
        createFolders = true,
      )
      println(s"Copied ${relativePathInTemplateDirectory}")
    }

    destinationGit.add().addFilepattern(".").call()

    for {
      (submodule, relativePathInTemplateDirectory) <-
        submodulesInTemplateDirectory
    } addSubmodule(
      destinationGit,
      relativePathInTemplateDirectory,
      submodule.remoteUrl,
      submodule.id.getName(),
      submodule.path,
    )

    val relativeSubmoduleDirectory = os.rel / "submodule"

    for {
      (gitReference, _) <- sourcesOutsideTemplateDirectoryToSymlinksMap
      submodule <- gitReference match {
        case s: Submodule => Some(s)
        case _            => None
      }
    } addSubmodule(
      destinationGit,
      relativeSubmoduleDirectory / submodule.path.last,
      submodule.remoteUrl,
      submodule.id.getName(),
      submodule.path,
    )

    val relativeDestinationTemplateGitDirectory =
      relativeSubmoduleDirectory / templateGitRootDirectory.last
    val destinationTemplateGitDirectory =
      destinationDirectory / relativeDestinationTemplateGitDirectory
    addSubmodule(
      destinationGit,
      relativeDestinationTemplateGitDirectory,
      templateGit.remoteUrl.toString(),
      templateGit.headId.getName(),
      templateGitRootDirectory,
    )

    for {
      (symlink, gitReference) <- symlinksToSourceMap
      (gitReferenceRootDirectory, relativeGitDirectory) <- gitReference match {
        case Repository(_, _, _) =>
          Some(
            (templateGitRootDirectory, relativeDestinationTemplateGitDirectory)
          )
        case Submodule(path, _, _, _) =>
          if (path.startsWith(templateDirectory)) then None
          else
            Some(
              (path, relativeSubmoduleDirectory / path.last)
            )
      }

      symlinkTargetRelativeToTemplateGit = symlink.target.relativeTo(
        gitReferenceRootDirectory
      )

      absoluteSymlinkTarget =
        destinationDirectory / relativeGitDirectory / symlinkTargetRelativeToTemplateGit

      symlinkNameRelativeToTemplateDirectory = symlink.name.relativeTo(
        templateDirectory
      )
      absoluteSymlinkName =
        destinationDirectory / symlinkNameRelativeToTemplateDirectory

      relativeSymlinkTarget = absoluteSymlinkTarget.relativeTo(
        absoluteSymlinkName / os.up
      )
    } {
      os.makeDir.all(absoluteSymlinkName / os.up)
      os.symlink(absoluteSymlinkName, relativeSymlinkTarget)
      println(
        s"Created symlink ${symlinkNameRelativeToTemplateDirectory} → ${relativeSymlinkTarget} (${absoluteSymlinkTarget})"
      )
    }

    destinationGit.add().addFilepattern(".").call()

    // TODO: We could add some more information to the initial commits message, like the LaTeM version.
    destinationGit.commit().setMessage(s"Instantiated ${templateDirectory.last}").call()
  }

  def submoduleStatus(status: org.eclipse.jgit.submodule.SubmoduleStatus) =
    s"${status.getHeadId()} ${status.getIndexId()} ${status.getPath} ${status.getType}"

  def addSubmodule(
      git: Git,
      path: os.RelPath,
      uri: String,
      commit: String,
      localPath: os.Path,
  ) = {
    val initialRemote = {
      val localPathDotGitDir = localPath / ".git"
      if (localPathDotGitDir.toIO.exists) then localPathDotGitDir.toString
      else uri
    }

    val addMessage = {
      var res = s"Add git submodule ${path} (${uri}, ${commit})"
      if (initialRemote != uri)
        res += s" via ${initialRemote}"
      else
        res += ". WARNING: Remote clone as repository is not available locally."
      res
    }
    println(addMessage)

    val gitWorkTree = os.Path(git.getRepository.getWorkTree)
    val submodulePath = gitWorkTree / path
    val submoduleAdded = {
      // Always try to use 'git' directly first instead of jgit, for
      // two reasons:
      // 1. jgit does not support --depth (yet), git does.
      // 2. jgit does not support cloning from a local filesystem, git does.
      // format: off
      val gitSubmoduleAddCommand = mutable.ListBuffer[os.Shellable](
        "git",
        "-C", gitWorkTree,
        // Newer git versions disallow local clones due to
        // https://github.com/git/git/security/advisories/GHSA-3wp6-j8xr-qw85
        // Re-allow them here, as we assume that the templates are trusted.
        "-c", "protocol.file.allow=always",
        "submodule", "add",
      )
      if (initialRemote != localPath.toString) {
        // Perform shallow clone if the we use the remote URL for cloning.
        gitSubmoduleAddCommand ++= List("--depth", "10")
      }
      gitSubmoduleAddCommand ++= List(
        "--",
        initialRemote, path,
      )
      // format: on
      val res = os.proc(gitSubmoduleAddCommand).call()
      res.exitCode == 0
    }

    val submoduleGit =
      if (!submoduleAdded) {
        // Note that we use here uri, not initialRemote, as jgit
        // appears to be not able to clone from local path.
        val repo = git
          .submoduleAdd()
          .setPath(path.toString)
          .setURI(uri)
          .call()
        new Git(repo)
      } else getGitFor(gitWorkTree / path)

    try {
      {
        def checkout(git: Git, commit: String) =
          git.checkout().setName(commit).call()
        try {
          checkout(submoduleGit, commit)
        } catch {
          case refNotFoundException @ (_: jgit.api.errors.RefNotFoundException | _: org.eclipse.jgit.api.errors.JGitInternalException) => {
            println(
              s"Failed to checkout ${commit} from ${submoduleGit}, attempting to re-fetch"
            )
            if (initialRemote == uri) {
              // The shallow clone from the HEAD probably did not
              // retrieve the particular commit, try to explicitly
              // fetch it from the remote.
              // format: off
              os.proc(
                "git",
                "-C", submodulePath,
                "fetch",
                "origin", commit
              ).call()
              // format: on
            }

            checkout(submoduleGit, commit)
            submoduleGit.fetch().call()
          }
        }
      }

      println(s"submodule update init ${localPath}")
      val res = os.proc("git",
        "-C", submodulePath,
        "submodule", "update",
        "--init",
        "--recursive",
        "--depth", "10",
      ).call()

      // TODO: if we cloned with jgit, then the submodules URL may be
      // already uri, so we should check for that.
      if (initialRemote != uri) {
        // format: off
        val res = os.proc("git",
          "-C", gitWorkTree,
          "submodule", "set-url",
          "--",
          path, uri,
        ).call()
        // format: on
      }
    } finally {
      closeGit(submoduleGit)
    }

    git
      .add()
      .addFilepattern(path.toString)
      .call()

    println(s"Successfully added submodule at ${path}")
  }
}
