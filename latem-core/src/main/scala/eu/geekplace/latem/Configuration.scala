package eu.geekplace.latem

case class Configuration(
    templateDirectory: os.Path,
    destinationDirectory: os.Path,
)
