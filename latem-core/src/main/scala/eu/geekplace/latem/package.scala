// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright © 2021 Florian Schmaus
package eu.geekplace

package object latem {
  def sayFoo() = println("I say 'foo'")
}
