GRADLE ?= ./gradlew

.DEFAULT_GOAL := check

.PHONY: submodules
submodules:
	git submodule update --init --recursive --depth 10

.PHONY: build
build:
	$(GRADLE) build

.PHONY: check
check:
	$(GRADLE) check

.PHONY: format
format:
	# Does not really work with Scala 3 constructs.
	$(GRADLE) scalafmt

.PHONY: test
test: test-runner

.PHONY: test-runner
test-runner:
	./test-runner $(TESTSUITE_RUNNER_ARGS)

.PHONY: test-runner-debug
test-runner-debug:
	TESTSUITE_RUNNER_ARGS="-d" $(MAKE) test-runner

.PHONY: distclean
distclean:
	git clean -xdf
	git submodule foreach --recursive git clean -xdf
