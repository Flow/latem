package eu.geekplace.latem.cli

object Cli {
  def main(args: Array[String]) = {
    val configuration = new Configuration(args.toIndexedSeq)

    if (configuration.debug())
      println(s"latem ${args}")

    eu.geekplace.latem.LaTeM.instantiateTemplate(configuration.libConfiguration)
  }
}
