package eu.geekplace.latem.cli

import org.rogach.scallop._

private val osPathListConverter = listArgConverter[os.Path](s => os.Path(s))
private val osFilePathConverter =
  singleArgConverter[os.FilePath](s => os.FilePath(s))
private implicit val osPathConverter: ValueConverter[os.Path] =
  singleArgConverter[os.Path](s => os.Path(s))

class Configuration(arguments: Seq[String]) extends ScallopConf(arguments) {
  val debug = opt[Boolean](
    short = 'd',
  )
  val verbose = opt[Boolean](
    short = 'v',
  )

  val templateSearchPaths = opt[List[os.Path]](
    default = sys.env.get("LATEM_TEMPLATE_SEARCH_PATH").map(os.Path(_)).map(List(_))
  )(osPathListConverter)

  // This helps to work around working directory handling when gradle
  // is invoked, which will cause the cwd to be in latem-cli/.
  val workingDirectory = opt[os.Path](
    default = sys.env.get("LATEM_WORKING_DIRECTORY").map(os.Path(_))
  )

  val template = trailArg[os.FilePath](
    descr =
      "The template to use. Either an absolute directory, or a directory relative to the template search path or the working directory."
  )(osFilePathConverter)

  val destination = trailArg[os.FilePath](
    descr =
      "The absolute or relative destination directory to instanciate the template in."
  )(osFilePathConverter)

  verify()

  lazy val templateDirectory = template() match
    case absolutePath: os.Path => absolutePath
    case relativePath => {
      val searchPaths = (templateSearchPaths.getOrElse(List()) :+ workingDirectory())
      searchPaths
        .map(p => p / os.RelPath(relativePath.toString))
        .filter(_.toIO.isDirectory)
        .headOption
        .getOrElse(
          throw new IllegalArgumentException(
            s"No template named ${relativePath} found in ${searchPaths}"
          )
        )
    }

  lazy val libConfiguration = {
    val destinationDirectory = os.Path(destination(), os.pwd)
    eu.geekplace.latem.Configuration(templateDirectory, destinationDirectory)
  }
}
