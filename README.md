# LaTeM - The *La*TeX *Te*mplate *M*anager

Create LaTeX documents by instanciating templates.

```bash
$ ./latem paper-acmart ~/papers/my-novel-idea
```

## License

This project is licensed under the MIT License. See the file `LICENSE`
in the project's root directory.
